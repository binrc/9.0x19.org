<pre>
cpu% man 1 futility

     FUTILITY(1)                                            FUTILITY(1)

     NAME
          futility - introduction to Plan 9

     DESCRIPTION
          ABANDON ALL HOPE YE WHO ENTER HERE. 

          9.0x19.org is a an exercise in futility. It exists for the sole
	  purpose of allowing the admin to watch it all burn. Perhaps it 
	  it is the insecurities that made him do this. Maybe it is just 
	  another project that will be left to rot. Only time can tell. 
    
     SEE ALSO
          despair(1)
	  confusion(1)

cpu% 
</pre>

