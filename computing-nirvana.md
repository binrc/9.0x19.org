# Quality Operating Systems

- [OpenBSD](https://openbsd.org)
- [9shit](http://9front.org)

# Graphical User Incompetences

- [Suckless](https://suckless.org)
- use rio
- tmux

# Editors

- vim
- sam

# Langs

- [POSIX sh](https://pubs.opengroup.org/onlinepubs/9699919799/)
- [POSIX C](https://archive.org/details/TheCProgrammingLanguageFirstEdition)
- [rc](http://9.0x19.org/papers/rc)
- [9 C](http://9.0x19.org/papers/comp)

# Build systems

- make
- mk

# Web

- [thisisamotherfuckingwebsite](http://motherfuckingwebsite.com)


