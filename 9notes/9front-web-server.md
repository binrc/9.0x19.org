# 9front on the web

# Vultr

1.  upload iso
2.  add vps
3.  open vnc console
4.  install as normal
5.  reboot
6.  remove iso
7.  reboot again

------------------------------------------------------------------------

# set up a CPU server

## Add users to file server

    % con -C /srv/cwfs.cmd 
    newuser <ExampleUser> 
    newuser sys +<ExampleUser>
    newuser adm +<ExampleUser>
    newuser upas +<ExampleUser>

## set up environment

reboot, set `user=<ExampleUser>` for the bootarg, then run
`/sys/lib/newuser`

## set up headless booting

    % 9fs 9fat

edit `/n/9fat/plan9.ini`

    bootfile=9pc64
    nobootprompt=local!/dev/sdC0/fscache
    mouseport=ps2
    monitor=vesa
    vgasize=1024x768x14
    user=<ExampleUser>
    tiltscreen=none
    service=cpu

## Add hostonwer info to nvram

    % nvram=/dev/sdF0/nvram auth/wrkey
    bad nvram des key
    bad authentication id
    bad authentication domain
    authid: <ExampleUser>
    authdom: 9front
    secstore key: <press the return key if you do not want to type this at boot time>
    password: <make it 8 chars>

## Auth server

    % auth/keyfs
    % auth/changeuser <ExampleUser>
    Password: <what you put earlier>
    Confirm password:
    Assign new Inferno/POP secret? [y/n]: n
    Expiration date (YYYYMMDD or never) [never]: never
    Post id:
    User's full name: 
    Department #:
    User's email address:
    Sponsor's email address:
    user <ExampleUser> installed for Plan 9

## Add permissions

append to `/lib/ndb/auth`

    hostid=<ExampleUser>
    uid=!sys uid=!adm uid=*

then reboot

## Test if it worked with drawterm

    $ /opt/drawterm -u binrc -h example.com -a example.com

------------------------------------------------------------------------

# set up rc-httpd

edit `/rc/bin/rc-httpd/select-handler`

    #!/bin/rc
    PATH_INFO=$location

            switch($SERVER_NAME) {
            case example.com
                   FS_ROOT=/usr/binrc/www/$SERVER_NAME
                   exec static-or-index

            case *
                  error 503
    }

To listen on port 80 and run the handler on port 80:

    % cp /rc/bin/service/!tcp80 /rc/bin/service/tcp80
    % chmod +x /rc/bin/rc-httpd/select-handler

Reboot and test.

------------------------------------------------------------------------

# set up werc

    % cd  
    % mkdir /sys/www && cd www 
    % hget http://werc.cat-v.org/download/werc-1.5.0.tar.gz 
    % tar xzf werc-1.5.0.tar.gz  
    % mv werc-1.5.0 werc 
     
    # ONLY DO THIS IF YOU *MUST* RUN THE THINGS THAT ALLOW WERC TO WRITE TO DISK 
    # EG. DIRDIR, BLAGH, ETC
    # DON'T DO THIS, JUST USE DRAWTERM OVER THE NETWORK
    # HTTP CLIENTS SHOULD NEVER BE ALLOWED TO WRITE TO DISK
    # PLEASE I BEG YOU
    % cd .. && for (i in `{du www | awk '{print $2}'}) chmod 777 $i 
     
    % cd werc/sites/ 
    % mkdir example.com 
    % mv default.cat-v.org example.com 

now re-edit `/rc/bin/rc-httpd/select-handler`

    #!/bin/rc 
    WERC=/sys/www/werc 
    PLAN9=/ 
    PATH_INFO=$location 
    switch($SERVER_NAME){ 
    case cirno 
            FS_ROOT=$WERC/sites/$SERVER_NAME 
            exec static-or-cgi $WERC/bin/werc.rc 
    case * 
            error 503 
    } 

------------------------------------------------------------------------

# SSL

no.

Actually, self signed is a yes.

Generate and install:

    % ramfs -p
    % cd /tmp
    % auth/rsagen -t 'service=tls role=client owner=*' > key
    % chmod 600 key
    % cp key /sys/lib/tls/key 
    % auth/rsa2x509 'C=US CN=example.com` /sys/lib/tls/key | auth/pemencode CERTIFICATE > /sys/lib/tls/cert
    % mkdir /cfg/$sysname
    % echo 'cat /sys/lib/tls/key >> /mnt/factotum/ctl' >> /cfg/$cycname/cpustart

Now add a listener in `/rc/bin/service/tcp443`:

    #!/bin/rc
    exec tlssrv -c /sys/lib/tls/cert -l /sys/log/https /rc/bin/service/tcp80 $*

And make it executable:

    % chmod +x /rc/bin/service/tcp443
