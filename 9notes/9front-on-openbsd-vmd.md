<title>9front vm on OpenBSD's vmd(8)</title>

# Why re-write the wheel?

- those who can do. Those who can't write about it. 
- 9front wiki sucks
- plan9port sucks
- I can't tell if the openbsd faq is intentionally vague sometimes

# The setup

shell commands: 

    daedalus.localnode# mkdir -p /var/vmd && cd /var/vmd
    daedalus.localnode# vmctl create -s 25G 9front.img


## vm

Write a configuration file for  `/etc/vm.conf`

    #   $OpenBSD: vm.conf,v 1.11 2021/11/11 09:38:14 claudio Exp $
    
    vm "9front"{
        disable 
        memory 2048M
        disk "/var/vmd/9front.img"
        cdrom "/var/vmd/9front-goldenage.iso"
        owner root
        interface{
            switch "local"
        }
    }
    
    switch "local" {
        interface bridge0
    }

## networking

2 interfaces are needed: `vether0` and `bridge0`:

`/etc/hostname.vether0`

    inet 10.0.0.1 255.255.255.0

`/etc/hostname.bridge0`

    add vether0

ip forwarding should also be enabled if the vm should be allowed to talk to the internet: 

`/etc/sysctl.conf`

    net.inet.ip.forwarding=1
    net.inet6.ip6.forwarding=1

A dhcp server is also needed so that the guests can automatically configure networking. An alternative option is to use static addresses. 

`/etc/dhcpd.conf`

    #	$OpenBSD: dhcpd.conf,v 1.1 2014/07/11 21:20:10 deraadt Exp $
    # vmd
    subnet 10.0.0.0 netmask 255.255.255.0 {
    	option routers 10.0.0.1;
    	range 10.0.0.2 10.0.0.255;
    	option domain-name "vmd.my.domain";
    	option domain-name-servers 1.1.1.1;
    }

And finally, all the pieces must be assembled using pf.conf. This configuration file contains extra things that aren't strictly necessary for virtualizatoin with networking. 

`/etc/pf.conf`

    #	$OpenBSD: pf.conf,v 1.55 2017/12/03 20:40:04 sthen Exp $
    
    set skip on lo0
    
    # hypervisor config, allow SSH and DHCP. 
    # Note: this configuration can cause your system to become a rogue 
    # DHCP service on the external LAN if your dhcpd flags contain more interfaces than 'vether0'
    tcp_services="{ssh bootps bootpc}"
    udp_services="{bootps bootps}"
    block in on {em0 iwn0}
    pass in proto tcp to any port $tcp_services keep state
    pass in proto udp to any port $tcp_services keep state
    pass out all
    
    # By default, do not permit remote connections to X11
    block return in on ! lo0 proto tcp to port 6000:6010
    
    # Port build user does not need network
    block return out log proto {tcp udp} user _pbuild
    
    # VMD config
    pass out on egress from 10.0.0.0/24 to any nat-to (egress)
    pass in proto { udp tcp } from 10.0.0.0/24 to any port domain rdr-to 1.1.1.1 port domain

Run some commands to make things work: 

    daedalus.localnode# sysctl net.inet.ip.forwarding=1
    daedalus.localnode# sysctl net.inet6.ip6.forwarding=1
    daedalus.localnode# rcctl enable dhcpd vmd
    daedalus.localnode# rcctl set dhcpd flags vether0
    daedalus.localnode# rcctl start dhcpd vmd
    daedalus.localnode# pfctl -e /etc/pf.conf

# installing 9front

Upon starting the system you will need to mash the spacebar and set the following boot options:

    >console=0
    >display=none
    >boot

The rest of the installation is well documented at [9front.org](http://9front.org). If you want drawterm you need to [configure and run the vm as a CPU server](http://9.0x19.org/9notes/9front-web-server). You will need to know how to use ed(1), the standard editor. tourists need not apply. 

And now drawterm should run. 

![](9-guest.png)

To power off the vm you must run `fshalt` on the guest then `vmctl stop 9front` on the host. 'check tags' incoming. 

    halting...halted at Mon May 29 05:09:29 2023.
    /srv/cwfs.newfs.cmd...
    done halting
    rebooting...
    cpu0: exiting
    Takes a licking and keeps on ticking...
    can't reset

I hate 9 so much it's unreal. 
