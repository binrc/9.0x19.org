# (hypothetical) wifi 

If the pi's bwfm chip was supported, this is how (I assume) it would work: 

<pre>
term% cd /lib/firmware
term% hget http://firmware/openbsd.org/firmware/7.2/bwfm-firmware-20200316.1.3p3.tgz > firmware.tgz
term% tar xzvf firmware.tgz
term% cd firmware/
term% mv ./* ../
term% reboot
term% bind -a '#l1' /net
term% aux/wpa -s FCC_VIOLATING_WLAN -p /net/ether1
! Addidng key: proto=wpapsk essid=FCC_VIOLATING_WLAN
password: *******
!
term% ip/ipconfig ether /net/ether1
</pre>

Until then, it's ethernet only (unless you can find a supported dongle).

# Official raspi displays

I have a pi3 display and it's upside down. In order to fix this, do the following: 

<pre>
term% 9fs pidos
term% cd /n/pidos
term% sam config.txt
</pre>

and append the line: 

<pre>
lcd_rotate=2
</pre>


# automatic booting

<pre>
term% 9fs pidos
term% cd /n/pidos
term% sam cmdline.txt
</pre>


Note: you must use spaces, everything after the first newline seems to be ignored. This is in direct contrast to a `plan9.ini` on an i386 or amd64 installation where newlines are required. For all intents and purposes, `cmdline.txt` is the pi equivalent to a `plan9.ini`. 

<pre>
console=0 nobootprompt=local!/dev/sdM0/fs user=glenda
</pre>

Configuring 9 to automatically submit a DHCP request when it boots (so I don't have to run `ip/ipconfig` manually every time) and query an NTP server (because it's not 1970): 

<pre>
term% cp /adm/timezone/GMT /adm/timezone/local
term% sam /usr/glenda/lib/profile
</pre>

My profile looks something like: 

<pre>
[lots of stuff]
webfs
plumber
ip/ipconfig ether /net/ether0
sleep 3 # because DHCP is SLOW
aux/timesync -n pool.ntp.org
[lots of other stuff]
</pre>

Configuring the system in the hostowner's profile is hacky but it seems to work

# upgrading the system

<pre>
# get sources
sysupgrade

# build userland
cd /
. /sys/lib/rootstub
cd /sys/src
mk install
mk clean

# build manpages
cd /sys/man
mk

# build papers
cd /sys/doc
mk
mk html

# build kernel
cd /sys/src/9/bcm64
mk install
reboot
</pre>

# Images

<img src="pi-guts1.png">
<img src="pi-guts2.png">
<img src="pi-guts3.png">
<img src="pi-screen-case.png">
