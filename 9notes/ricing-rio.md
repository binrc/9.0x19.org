# Wallpaper support

Convert image to plan 9 image: 

    % jpg -9t walpaper.jpg > wallpaper
    % resample -x 1366 -y 768 wallpaper > resized
    % mv resized wallpaper

Modify rio sources: 

    % mkdir $home/src/rio
    % dircp /sys/src/cmd/rio/ $home/src/rio
    % cd $home/src/rio
    % sam data.c

Go to the iconinit function and edit it. My funciton looks like this: 

<pre>
void
iconinit(void)
{
	int fp;
	Image *pape = nil;
	fp = open("/usr/glenda/lib/wallpaper", OREAD);

	if(fp){
		pape = readimage(display, fp, 0);
		close(fp);
	}
	
	if(pape){
		background = allocimage(display, Rect(0, 0, Dx(pape->r), Dy(pape->r)), RGB24, 1, 0x000000FF);
		draw(background, background->r, pape, 0, pape->r.min);
	} else {
		fprint(2, "iconinit failed: %r\nusing default instead\n");
		background = allocimage(display, Rect(0,0,1,1), screen->chan, 1, 0x777777FF);
	}

	/* greys are multiples of 0x11111100+0xFF, 14* being palest */
	cols[BACK] = allocimage(display, Rect(0,0,1,1), CMAP8, 1, 0xFFFFFFFF^reverse);
	cols[BORD] = allocimage(display, Rect(0,0,1,1), CMAP8, 1, 0x999999FF^reverse);
	cols[TEXT] = allocimage(display, Rect(0,0,1,1), CMAP8, 1, 0x000000FF^reverse);
	cols[HTEXT] = allocimage(display, Rect(0,0,1,1), CMAP8, 1, 0x000000FF);
	if(!reverse) {
		cols[HIGH] = allocimage(display, Rect(0,0,1,1), CMAP8, 1, 0xCCCCCCFF);
		titlecol = allocimage(display, Rect(0,0,1,1), CMAP8, 1, DGreygreen);
		lighttitlecol = allocimage(display, Rect(0,0,1,1), CMAP8, 1, DPalegreygreen);
	} else {
		cols[HIGH] = allocimage(display, Rect(0,0,1,1), CMAP8, 1, DPurpleblue);
		titlecol = allocimage(display, Rect(0,0,1,1), CMAP8, 1, DPurpleblue);
		lighttitlecol = allocimage(display, Rect(0,0,1,1), CMAP8, 1, 0x222222FF);
	}
	dholdcol = allocimage(display, Rect(0,0,1,1), CMAP8, 1, DMedblue);
	lightholdcol = allocimage(display, Rect(0,0,1,1), CMAP8, 1, DGreyblue);
	paleholdcol = allocimage(display, Rect(0,0,1,1), CMAP8, 1, DPalegreyblue);
	paletextcol = allocimage(display, Rect(0,0,1,1), CMAP8, 1, 0x666666FF^reverse);
	sizecol = allocimage(display, Rect(0,0,1,1), CMAP8, 1, DRed);

	if(reverse == 0)
		holdcol = dholdcol;
	else
		holdcol = paleholdcol;
}
</pre>

Build and test the new rio in a nested display: 

    % mk
    % ./6.out

Install new rio: 

    % mk install


<img src="rio-wallpaper.png" style="max-width: 60%;">


# Theme

no.